| Branch                       | Pipeline                                                                                                                                                           | Coverage                                                                                                                                                           | Docs |
|------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|
|  `master` (`stable` release) | [![pipeline status](https://gitlab.com/crypto-toolbox/bitex-rest-api/badges/master/pipeline.svg)](https://gitlab.com/crypto-toolbox/bitex-rest-api/commits/master) | [![coverage report](https://gitlab.com/crypto-toolbox/bitex-rest-api/badges/master/coverage.svg)](https://gitlab.com/crypto-toolbox/bitex-rest-api/commits/master) |  N/A |
| `dev` (`nightly` release)    | [![pipeline status](https://gitlab.com/crypto-toolbox/bitex-rest-api/badges/dev/pipeline.svg)](https://gitlab.com/crypto-toolbox/bitex-rest-api/commits/dev)       | [![coverage report](https://gitlab.com/crypto-toolbox/bitex-rest-api/badges/dev/coverage.svg)](https://gitlab.com/crypto-toolbox/bitex-rest-api/commits/dev)       | N/A  |

# BitEx-REST-API Package

The BitEx-REST-API Package is a library implementing REST APIs for a variety of crypto exchanges.
It offers low-level access to APIs and implements public (non-authenticated) and private
(authenticated) endpoints of each exchange implemented.


# Supported REST APIs by Exchange

| Exchange             | Public API | Private API | Test Suite |
|----------------------|------------|-------------|------------|
| Bitfinex             | DONE       | DONE        | DONE       |
| Bitstamp             | DONE       | DONE        | DONE       |
| Bittrex              | DONE       | DONE        | DONE       |
| Bter                 | DONE       | DONE        | DONE       |
| C-CEX                | DONE       | DONE        | DONE       |
| CoinCheck            | DONE       | DONE        | DONE       |
| Cryptopia            | DONE       | DONE        | DONE       |
| HitBTC               | DONE       | DONE        | DONE       |
| Kraken               | DONE       | DONE        | DONE       |
| OKCoin               | DONE       | DONE        | DONE       |
| Poloniex             | DONE       | DONE        | DONE       |
| QuadrigaCX           | DONE       | DONE        | DONE       |
| The Rock Trading LTD | DONE       | DONE        | DONE       |
| Vaultoro             | DONE       | DONE        | DONE       |
| Quoine               | Planned    | Planned     | Planned    |
| ITBit                | Planned    | Planned     | Planned    |

# Installation

Manually, using the supplied `setup.py` file:

`python3 setup.py install`

or via pip

`pip install bitex_rest_api`

# Further documentation

Further documentation can be found [here](http://bitex.readthedocs.io/en/release-2.0.0/).

# Disclaimer

Due to technical reasons I cannot test all private methods of each exchange,
since some of them need me to move actual currency - exposing an API Key of an account with
actual value is, as you may imagine, a security risk and thus isn't done.

Therefore, take great care when using the library. I cannot be
held responsible for any losses or damages you suffer while using this library.
