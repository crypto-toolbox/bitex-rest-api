Development Cycle
=================

Workflow
--------
The general workflow can be described as follows:

    1. An issue is created, describing the task to be completed (feature, issue, release)
    2. A branch is created for the corresponding issue, and work is committed to it.
    3. Once work is complete, a merge request may be opened (if not already done so) and the
       work submitted is reviewed.
    4. Should the merge request be accepted, and the CI tests pass (linters and unittests), the
       branch is merged into `dev`_ (or into `release`_  if its a bugfix and the branch exists,
       and additionally into `master`_ if it's a critical bugfix (hotfix)).
    5. Once new release is scheduled, the `dev`_ branch will be forked and a new `release`_ branch will
       be created.
    6. A preview release is published to pypi, built from the `release`_ branch, and the package
       may be tested by the public.
    7. Should bugs arise, new issues need to be created for these, and steps 2-4 executed; the
       bugfix will then be merged into the `release`_ branch.
    8. Once testing and bugfixing has taken place and the `release`_ branch is deemed stable, a stable
       release is scheduled.
    9. The `release`_ branch will now be merged into the `master`_ branch, as well as back into
       the `dev`_ branch, and a stable release published to pypi.


Release Types
-------------

There are three release types that are part of our development cycle:

    - *nightly* releases (X.Y.Z**a**W)
    - *preview* releases (X.Y.Z**rc**W)
    - *stable* releases (X.Y.Z)

*nightly* releases are, as the name indicates, built once a day (during the night) and are built
and packaged with the code currently on `dev`_ - while tests and linter checks are run, this package
is built regardless of their result. This means that `nightly` builds are very unstable and should
be used with caution. Each new *nightly* release increments the iteration (`W` in the above list's
examples) by one. They are built from the `dev`_ branch.

*preview* releases are what other project may call *release candidates* - they contain a fixed set
of new features, and only bugfixes are merged onto their related branches. They are considered
*stable*, as such a release is only built if all linter checks and unittests pass. Each new
*preview* release increments the iteration (`W` in the above list's
examples) by one. They are built from `release-X.Y`_ branches.

*stable* releases are fully tested versions of the project, and are considered production ready and
stable. Their branches only accept hotfixes, which are critical to ensure the correct execution
of the code contained within them. Depending on the type of release, which may be either a
*bugfix release*, or a *feature release*, the version's *patch* number (`Z`) or minor number (`Y`) gets
incremented by one. A *major* version bump is only done if there are backwards compatible changes
in the new release. They are built from the `master`_ branch.

Branching Model
===============
The BitEx project uses strict rules when it comes to naming conventions, code changes and merge
requests. By default, all branches are **protected** - This means, no one can directly push commits
to them. All changes need to be submitted as merge requests, checked for fulfilled
`Merge Request Submission Requirements`_, reviewed by the author and/or maintainers and approved
by them.

Branch Types
-------------
There are five basic branches present on the git repository:

    - `dev`_ (`nightly` pypi release)
    - `master`_ (`stable` pypi release)
    - `release-X.Y`_ (`preview` pypi release)
    - `feat-#XYZ`_ (project features)
    - `issue-#XYZ`_ (project bugfixes, if forked from `dev`_ or `release-X.Y`_)
    - `issue-#XYZ`_ (project hotfix, if forked from `master`_)

A closer look at each of these branches is offered in the following sections.

_`dev` - Development Branch (`nightly` releases)
-----------------------------------------------
All development of features and non-critical bugs happens on this branch.

Lifespan: Permanent

Forks from: `master`_

Merges into: None

Accepts merges from branches of type:

    - `feat-#XYZ`_
    - `issue-#XYZ`_ (bugfixes and hotfixes)
    - `release-X.Y`_

_`master` - Stable Branch (`stable` releases)
--------------------------------------------
This branch only takes merges from the release branch, or from branches which
introduce critical bug fixes (hotfixes).

Lifespan: Permanent

Forks from: None

Merges into: None

Accepts merges from branches of type:

    - `release-X.Y`_
    - `issue-#XYZ`_ (hotfixes only)

_`release-X.Y` - Release Branch (`preview` release)
--------------------------------------------------
This is a temporary branch; it is only created when a new stable version is to be released, and
destroyed once it has been merged into master. Only bugfixes are developed on this branch.

Lifespan: Temporary, until merged

Forks from: `dev`_

Merges into: `dev`_, `master`_

Accepts merges from branches of type:

    - `issue-#XYZ`_ (bugfixes and hotfixes)

_`feat-#XYZ` - Feature submissions
-----------------------------------------------
Lifespan: Temporary, until merged

Forks from: `master`

Merges into: None

Accepts merges from branches of type: None

_`issue-#XYZ` - Bugfix and Hotfix submissions
-----------------------------------------------
Lifespan: Temporary, until merged

Forks from:

    - `release-X.Y`_ or `dev`_ (Bugfixes)
    - `master`_ (Hotfixes)

Merges into:

    - `release-X.Y`_ or `dev`_ (Bugfixes)
    - `master`_, `release-X.Y`_, `dev`_ (Hotfixes)

Accepts merges from branches of type: None

_`Submitting Issues`
====================

_`Issue Template`
-----------------
You're encourage to use the following template when filing issues:

Issue Title:

    A Short description of the issue you're reporting.

Issue Body:

    Long description of the issue you're reporting, but keep in mind that bullet points and short
    paragraphs are easier to read!

    Also, always provide a minimal, working code example to reproduce the issue. Do **NOT** submit
    screenshots, but instead use markdown to format code, or submit large files via pastebin.


    Steps to reproduce:

    - Step 1
    - Step 2
    - Step 3
    - Error!

    Code Example:

    ```python

    import mylib

    class NewClass(mylib.OldClass):
        def __init__(a, b):
            print( a + b )

    if __name__ == '__main__':
        instance = NewClass(1234, 'Superstring')

    ```

    Stacktrace and/or Error log:

    ```bash

    Traceback (most recent call last):
      File "/usr/lib/python3.5/code.py", line 91, in runcode
        exec(code, self.locals)
      File "<input>", line 1, in <module>
    TypeError: unsupported operand type(s) for +: 'int' and 'str'

    ```

    Python Version: Python 3.X

    Package Version: X.Y.Z

    Installed from: pypi or local git?




Submitting Merge Requests
=========================

_`Merge Request Submission Requirements`
----------------------------------------
All Merge Requests must fulfill the following requirements in order to be accepted:
    1. Code **must** be covered by unittests *completely*.
    2. Code **must** pass pydocstyle and pylint linters with provided configurations.
    3. Code **must** be documented properly; parameters, returned values and their types, as
       as well as explicitly raised `Exception`s must be described in `Sphinx reStructured Syntax`_.


_`Commit Submission Requirements`
---------------------------------
As a general rule of thumb, the following requirements should be fulfilled by all commits:

 1. Commits **must** be atomic.
 2. The *commit title* **must** start with a valid lable (see the `Label Reference`_ section
    for more information).
 3. The *commit title* **must** state a short and concise description of the change.
 4. The commit body **may** include further details of the change done in this commit.

From these rules, the following commit layout can be distilled::

    <LABEL> Description of the implemented change.

    Further details regarding the change, which is not bound
    by any specific requirement.


Code Labels
=============
Code labels are a way to categorize commits, branches and merge requests. They **must** be added in the
**commit title**, at the beginning of the file.


Merge Request Code Labels
-------------------------
Merge Requests, when merged, should contain the following labels in their commit title:

    - For features: "`[FEATURE-#XYZ]`_ <DESCRIPTION>."
    - For Hot- and Bugfixes: "`[PATCH-#XYZ]`_ <DESCRIPTION>."
    - For releases:
        - "`[RELEASE-X.Y]`_ Initial release."
        **OR**
        - "`[RELEASE-X.Y][PATCH]`_ Bugfix release."

.. NOTE::
    Note that we do not differentiate between hotfixes and bugfixes when labelling them during
    during the merge. Instead, we look at the branch from which they forked from: branches
    originating from `master`_ are hotfixes, while merge requests originating from any other branch
    are bug fixes.

Commit Code Labels
------------------
Commits should be labelled as follows:

    - "`[FIX]`_ <description>."
    - "`[UPDATE]`_ <description>."
    - "`[ADD]`_ <description>."
    - "`[REMOVE]`_ <description>."
    - "`[MOVE]`_ <description>."

These labels are required and are automatically enforced by gitlab. Make sure to keep your titles
concise, changes atomic and end them with a fullstop - for a list of requirements see the section
`Commit Submission Requirements`_, above.

The changelog for all releases is directly generated from these commit titles and will end up in
the release verbatim. The body of each commit is omitted.




_`Label Reference`
==================

_`Feature-Request` (Issue Label)
--------------------------------

_`Bug` (Issue Label)
--------------------

_`Question/Discussion` (Issue Label)
------------------------------------

_`Help Wanted` (Issue Label)
----------------------------

_`Critical` (Issue Label)
-------------------------

_`Third-party Issue` (Issue Label)
----------------------------------

_`Documentation` (Issue Label)
------------------------------

_`[FEATURE-#XYZ]` (Merge Commit Label)
--------------------------------------
    Used for feature branch merges. The issue reference number must be valid and refer to an
    **OPEN** issue on the repository's issues page.

_`[PATCH-#XYZ]` (Merge Commit Label)
------------------------------------
    Used for patch branch merges. The issue reference number must be valid and refer to an **OPEN**
    issue on the repository's issues page.

_`[RELEASE-X.Y]` (Merge Commit Label)
-------------------------------------
    Used for Release branch merges. The version number must only state MAJOR and MINOR
    versions, i.e. `1.5`, and should be equivalent to the version that's being released.

_`[RELEASE-X.Y][PATCH]` (Merge Commit Label)
--------------------------------------------
    Used for patch release branch merges. The version number must only state MAJOR and MINOR
    versions, i.e. `1.5`, and must be the version this patch is **FOR**.

_`[ADD]` (Change Commit Label)
------------------------------
    Tag file creation commits with this label.

_`[REMOVE]` (Change Commit Label)
---------------------------------
    Tag file removal commits with this label.

_`[MOVE]` (Change Commit Label)
-------------------------------
    Tag file movement commits with this label.

_`[FIX]` (Change Commit Label)
------------------------------
    Tag Code change commits with this label.

_`[UPDATE]` (Change Commit Label)
---------------------------------
    Tag updates to configs, settings, docstrings with this label.



_`Sphinx reStructured Syntax`: http://www.sphinx-doc.org/en/stable/rest.html