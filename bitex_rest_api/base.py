"""API Meta Classes for bitex_rest_api.

Each class is required to access an exchange's API, and must be subclassed before usage.

A minimal, working example::

    from bitex_rest_api.base import BitexAPIABC, BitexSessionABC, BitexAuthABC

    class BitexAuthSubClass(BitexAuthABC):
        def __init__(key, secret):
            super(BitexSessionSubclass, self).__init__(auth)

        def __call__(request):
            request.headers = {'SUPER-SECRET': (self.secret_as_bytes + self.key_as_bytes).encode()}


    class BitexSessionSubclass(BitexSessionABC):
        def __init__(key, secret):
            auth = BitexAuthSubclass(key, secret)
            super(BitexSessionSubclass, self).__init__(auth)


    class BitexAPISubclass(BitexAPIABC):
        def __init__(key, secret):
            private_session = BitexSessionSubclass(key, secret)
            addr = 'https://myexchange.io/api'
            version = 'v1'
            timeout = 12
            super(BitexSessionSubclass, self).__init__(addr, version, timeout, auth)

We subclass the ABC classes defined in this file, and add a custom header to the request in
BitexAusSubclass's __call__() method. This is added to all requests made to private endpoints
(triggered by passing the 'private=True' kwarg to  BitexAPISubclass's get(), put(), post(), etc.
methods).

"""
# Import Built-Ins
import logging
import warnings
import time
import abc
import pathlib

from urllib.parse import parse_qs

# Import Third-party
import requests

# Import Homebrew
from bitex_rest_api.exceptions import IncompleteCredentialConfigurationWarning

# Init Logging Facilities
log = logging.getLogger(__name__)


class BitexAuthABC(requests.auth.AuthBase, metaclass=abc.ABCMeta):
    """Authentication Meta Class for API authentication.

    Takes care of generating a signature and preparing data sent, headers and URLs as required
    by the exchange this class is subclassed for.
    """

    # pylint: disable=no-self-use

    def __init__(self, key, secret):
        """Initialize a BitexAuthABC instance.

        :param str key: API Key.
        :param str secret: API Secret.
        """
        self.key = key
        self.secret = secret

    @property
    def key_as_bytes(self):
        """Return the key encoded as bytes.

        :rtype: bytes
        """
        return self.key.encode('utf-8')

    @property
    def secret_as_bytes(self):
        """Return the secret encoded as bytes.

        :rtype: bytes
        """
        return self.secret.encode('utf-8')

    @abc.abstractmethod
    def __call__(self, request):
        """Sign the given request.

        This must be extended in subclasses, otherwise signing requests will likely not work,
        as it merely returns the request.

        :param requests.PreparedRequest request: The prepared request to sign.
        :rtype: requests.PreparedRequest
        """
        return request

    @staticmethod
    def decode_body(request):
        """Decode the urlencoded body of the given request and return it.

        Some signature algorithms require us to use the body. Since the body is already
        urlencoded by requests.PreparedRequest.prepare(), we need undo its work before
        returning the request body's contents.

        :param requests.PreparedRequest request: The request whose body we should decode.
        :rtype: Tuple[Tuple[str, str], ...]
        """
        body_as_dict = parse_qs(request.body)
        return tuple((key, value) for key, value in
                     sorted(body_as_dict.items(), key=lambda x: x[0]))

    @staticmethod
    def nonce():
        """Create a Nonce value for signature generation.

        By default, this is a unix timestamp with millisecond resolution converted to a str.
        :return: Nonce
        :rtype: str
        """
        return str(int(round(1000 * time.time())))


class BitexSessionABC(requests.Session, metaclass=abc.ABCMeta):
    """Custom requests.Session object for keep-alive http connections to private API endpoints.

    It may be extended to set default headers required for private connections, cookies and
    other values which may be set a default for each request.

    By default, it assigns a BitexAuthABC instance or a subclass thereof as the auth object to
    add to any requests made using this session.
    """

    @abc.abstractmethod
    def __init__(self, auth):
        """Initialize a session instance.

        :param bitex_rest_api.BitexAuthABC auth: callable authenticator instance.
        """
        super(BitexSessionABC, self).__init__()
        if not isinstance(auth, requests.auth.AuthBase):
            raise TypeError("'auth' parameter must be an instance of requests.auth.AuthBase!")
        self.auth = auth

    @property
    def key(self):
        """Return the Auth's key attribute value."""
        return self.auth.key

    @key.setter
    def key(self, value):
        """Set the Auth's key attribute value."""
        self.auth.key = value

    @property
    def secret(self):
        """Return the Auth's secret attribute value."""
        return self.auth.secret

    @secret.setter
    def secret(self, value):
        """Set the Auth's secret attribute value."""
        self.auth.secret = value


class BitexAPIABC(metaclass=abc.ABCMeta):
    """
    Meta Class defining a basic REST API interface.

    It provides several methods which extend functions found in the requests module. It also
    provides methods to create a URL to send queries to, by using pathlib to join the api base
    address, version and endpoint. urllib3.parse.urljoin isn't used here, since it only allows
    joining of a limited amount of parts.

    It maintains two requests.Session objects: one being a pure requests.Session instance used for
    requests to public endpoints (requiring no authentication), and another session instance passed
    in the constructor method, which is used for private endpoints (requiring authentication). The
    latter is optional, but will raise a IncompleteAuthConfigurationWarning if not passed, and
    sending requests to private endpoints will not work.
    """

    @abc.abstractmethod
    def __init__(self, addr, version=None, timeout=None, session=None):
        """Initialize a BitexAPIABC instance.

        :param str addr: API url
        :param str version: version of API to request
        :param int timeout: timeout to set for requests.
        :param requests.Sessions session: Requests session to make authenticated requests over.
        """
        self.addr = pathlib.Path(addr)
        self.version = pathlib.Path(version) if version else None
        self.timeout = timeout or 10
        self.private_connection = session
        if not session:
            warnings.warn("There was no Session for private endpoints passed - requesting private "
                          "endpoints won't work!", IncompleteCredentialConfigurationWarning)
        self.public_connection = requests.Session()

    def __enter__(self):
        """Implement simple context manager."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Exit the context."""

    def generate_url(self, endpoint):
        """Generate a Unique Resource Locator (API Address + URI).

        .. Note::

            In the context of this library we will use URL and URI interchangeably and define it as
            follows::

                (URI|URL) -> scheme:[//authority]path[?query][#fragment]

        :param str endpoint: Endpoint.
        :return: URL
        :rtype: pathlib.Path
        """
        endpoint_path = pathlib.Path(endpoint)
        if self.version:
            endpoint_path = self.version.joinpath(endpoint_path)
        return self.addr.joinpath(endpoint_path)

    def _request(self, method, endpoint, private=False, **request_kwargs):
        """Prepare the request and send it to the API.

        Requests sent may be entirely customized by request_kwargs, if so
        desired. Otherwise url, timeout and method will be set as given in ther related arguments.

        Some APIs require us to generate the signature for private requests using a string of
        path + query params (i.e. /endpoit?param1=true&params2=false). In order for these signatures
        to be accepted, a parameter order must be retained. Therefore, convert args params and data
        dicts to a list of 2-item-tuples, as requests only guarantees order-retainment for such
        structures.


        :param str method: valid HTTP Verb (GET, PUT, DELETE, etc.).
        :param str endpoint: The API endpoint to query, given as a path, sans version
        :param bool private: Whether or not to use the private connection.
        :param request_kwargs: kwargs for request.Request().
        :raises requests.ConnectionError: if self.private_connection is falsy.
        :rtype: request.Response
        """
        options = {'url': self.generate_url(endpoint), 'timeout': self.timeout, 'method': method}
        options.update(request_kwargs)

        if not isinstance(options.get('params', []), list):
            options['params'] = sorted(list(options['params'].items()), key=lambda x: x[0])

        if not isinstance(options.get('data', []), list):
            options['data'] = sorted(list(options['data'].items()), key=lambda x: x[0])

        if private:
            if not self.private_connection:
                raise requests.ConnectionError("No session object set for private connections - "
                                               "cannot make requests to private API endpoints!")
            return self.private_connection.request(**options)
        return self.public_connection.request(**options)

    def post(self, endpoint, private=False, **request_kwargs):
        """Issue a POST request to the API.

        :param str endpoint: The API Endpoint to query, sans base url and version.
        :param bool private: Whether or not to sign the request using the instance's authenticator.
        :param request_kwargs: any kwargs supported by requests.request.
        :rtype: requests.Response
        """
        return self._request('POST', endpoint, private, **request_kwargs)

    def get(self, endpoint, private=False, **request_kwargs):
        """Issue a GET request to the API.

        :param str endpoint: The API Endpoint to query, sans base url and version.
        :param bool private: Whether or not to sign the request using the instance's authenticator.
        :param request_kwargs: any kwargs supported by requests.request.
        :rtype: requests.Response
        """
        return self._request('GET', endpoint, private, **request_kwargs)

    def put(self, endpoint, private=False, **request_kwargs):
        """Issue a PUT request to the API.

        :param str endpoint: The API Endpoint to query, sans base url and version.
        :param bool private: Whether or not to sign the request using the instance's authenticator.
        :param request_kwargs: any kwargs supported by requests.request.
        :rtype: requests.Response
        """
        return self._request('PUT', endpoint, private, **request_kwargs)

    def delete(self, endpoint, private=False, **request_kwargs):
        """Issue a DELETE request to the API.

        :param str endpoint: The API Endpoint to query, sans base url and version.
        :param bool private: Whether or not to sign the request using the instance's authenticator.
        :param request_kwargs: any kwargs supported by requests.request.
        :rtype: requests.Response
        """
        return self._request('DELETE', endpoint, private, **request_kwargs)

    def head(self, endpoint, private=False, **request_kwargs):
        """Issue a HEAD request to the API.

        :param str endpoint: The API Endpoint to query, sans base url and version.
        :param bool private: Whether or not to sign the request using the instance's authenticator.
        :param request_kwargs: any kwargs supported by requests.request.
        :rtype: requests.Response
        """
        return self._request('HEAD', endpoint, private, **request_kwargs)

    def patch(self, endpoint, private=False, **request_kwargs):
        """Issue a PATCH request to the API.

        :param str endpoint: The API Endpoint to query, sans base url and version.
        :param bool private: Whether or not to sign the request using the instance's authenticator.
        :param request_kwargs: any kwargs supported by requests.request.
        :rtype: requests.Response
        """
        return self._request('PATCH', endpoint, private, **request_kwargs)

    def options(self, endpoint, private=False, **request_kwargs):
        """Issue a OPTIONS request to the API.

        :param str endpoint: The API Endpoint to query, sans base url and version.
        :param bool private: Whether or not to sign the request using the instance's authenticator.
        :param request_kwargs: any kwargs supported by requests.request.
        :rtype: requests.Response
        """
        return self._request('OPTIONS', endpoint, private, **request_kwargs)
