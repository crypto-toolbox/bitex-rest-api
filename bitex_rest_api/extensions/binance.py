"""Binance REST API backend.

Documentation available at:
    https://
"""

# Import Built-ins
import logging
import hashlib
import hmac

# Import Third-Party

# Import Homebrew
from bitex_rest_api.base import BitexAPIABC, BitexSessionABC, BitexAuthABC

log = logging.getLogger(__name__)


class BinanceAuth(BitexAuthABC):
    """Custom Auth object for the Binance exchange."""

    # pylint: disable=useless-super-delegation
    def __init__(self, key, secret):
        """Initialize a BinanceAuth instance."""
        super(BinanceAuth, self).__init__(key, secret)

    def __call__(self, request):
        """Sign request using Binance Authentication Algorithm.

        Binance requires the request's query parameters to be encoded using the API secret.
        Should the request be a GET, we can fetch the urlencoded string from the url by
        splitting it on `?`. If the method is POST, data is already urlencoded and exists in
        request.body. The resulting string must then either be appended to the request.url (GET) or
        send as the body of the request (POST).
        """
        if request.method == 'GET':
            url_sans_params, query_string = request.url.split('?')
            request.url = url_sans_params
        else:
            query_string = request.body
        signature = hmac.new(self.secret_as_bytes, query_string.encode('utf-8'), hashlib.sha256)
        hex_signature = signature.hexdigest()
        query_string += '&signature=' + hex_signature
        if request.method == 'GET':
            request.url += '?' + query_string
        else:
            request.body = query_string
        return request

    def prep_params(self, request):
        """Append the nonce to the parameters."""
        request.params.append(('timestamp', self.nonce()))
        return request


class BinanceSession(BitexSessionABC):
    """Custom Session object for the Binance exchagne."""

    def __init__(self, key, secret):
        """Initialize a BinanceSession instance and create a BinanceAuth instance."""
        auth = BinanceAuth(key, secret)
        super(BinanceSession, self).__init__(auth)
        self.headers = {'X-MBA-APIKEY': self.auth.key_as_bytes}


class BinanceAPI(BitexAPIABC):
    """Custom API object for interfacing with the Binance exchange."""

    def __init__(self, key=None, secret=None, version=None, addr=None, timeout=None):
        """Initialize the class instance.

        We force the default of `version` to None here as different endpoints require different
        versions. Keep this in mind when writing interfaces for this API, as it requires you to
        supply a version in the endpoint given to the http methods of this class.
        """
        addr = addr or 'https://api.binance.com/api'
        session_obj = BinanceSession(key, secret)
        super(BinanceAPI, self).__init__(
            addr=addr, version=version, timeout=timeout, session=session_obj
        )
