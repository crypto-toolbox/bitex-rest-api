"""Bittrex REST API backend.

Documentation available here:
    https://bittrex.com/home/api
"""
# Import Built-ins
import logging
import hashlib
import hmac

# Import Homebrew
from bitex_rest_api.base import BitexSessionABC, BitexAPIABC, BitexAuthABC

log = logging.getLogger(__name__)


class BittrexAuth(BitexAuthABC):
    """Custom Auth class for the Bittrex Exchange API."""

    def __call__(self, request):
        """Sign the request.

        Bittrex requires the request address, including query parameters, to be included as a
        sha512 encoded string in the request header.
        We must therefore append the api key and nonce to our url and generate the signature
        from it.

        .. Note:

            As of 21st of July, 2018, all requests to the Bittrex API are GET requests!

        """
        # Add api key and nonce to the query string.
        auth_query_params = 'apikey=' + self.key + "&nonce=" + self.nonce
        request.url += ('&' if '=' in request.url else '?') + auth_query_params

        # generate signature
        signature = hmac.new(self.secret.encode('utf-8'),
                             request.url.encode('utf-8'),
                             hashlib.sha512).hexdigest()
        request.headers.update({"apisign": signature})

        return request


class BittrexSession(BitexSessionABC):
    """Custom session class for the Bittrex Exchange API."""

    def __init__(self, key, secret):
        """Initialize the session instance."""
        auth = BittrexAuth(key, secret)
        super(BittrexSession, self).__init__(auth)


class BittrexAPI(BitexAPIABC):
    """Custom API class for accessing the Bittrex Exchange API."""

    def __init__(self, key, secret, addr='https://bittrex.com/api/', version='v1.1', timeout=None):
        """Initialize the API instance."""
        session = BittrexSession(key, secret)
        super(BittrexAPI, self).__init__(addr, version, timeout, session)
