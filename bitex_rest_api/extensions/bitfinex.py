"""Bitfinex REST API backend.

Documentation available at:
    https://docs.bitfinex.com/docs
"""
# pylint: disable=too-many-arguments
# Import Built-ins
import logging
import json
import hashlib
import hmac
import base64
from urllib.parse import parse_qs as parse_urlencoded_params

# Import Third-Party

# Import Homebrew
from bitex_rest_api.base import BitexAuthABC, BitexSessionABC, BitexAPIABC

log = logging.getLogger(__name__)


class BitfinexAuthLegacy(BitexAuthABC):
    """Custom Auth object for version 1 of the Bitfinex exchange API."""

    def __call__(self, request):
        """Generate proper authentication for the given request object."""
        nonce = self.nonce()
        _, target_resource = request.url.split('.com')
        if request.method in ('GET', 'HEAD'):
            params = parse_urlencoded_params(target_resource.split('?')[1])
        else:
            params = self.decode_body(request)
        params = [(key, value_list[0]) for key, value_list in params]
        params.append(('request', target_resource))
        params.append(('nonce', nonce))

        json_encoded_params = json.dumps(params)
        data = base64.standard_b64encode(json_encoded_params)
        signature = hmac.new(self.secret_as_bytes, data, hashlib.sha384).hexdigest()

        request.headers.update({"X-BFX-SIGNATURE": signature, "X-BFX-PAYLOAD": data})
        return request


class BitfinexAuthV2(BitexAuthABC):
    """Custom Auth object for version 2 of the Bitfinex exchange API."""

    def __call__(self, request):
        """Generate proper authentication for the given request object."""
        nonce = self.nonce()
        _, target_resource = request.url.split('.com')
        """
        On GET and HEAD requests, we need to need to generate the signature from the query
        parameters only. In all other cases, the endpoint needs to be included.
        """
        if request.method in ('GET', 'HEAD'):
            params = parse_urlencoded_params(target_resource.split('?')[1])
        else:
            params = parse_urlencoded_params(request.body)
        params = [(key, value_list[0]) for key, value_list in params]

        json_encoded_params = json.dumps(params)

        signature = '/api' + target_resource + nonce + json_encoded_params
        bytes_signature = signature.encode('utf-8')
        hmac_signature = hmac.new(self.secret_as_bytes, bytes_signature, hashlib.sha384)
        hex_sig = hmac_signature.hexdigest()
        auth_headers = {'bfx-signature': hex_sig, 'bfx-nonce': nonce}
        request.headers.update(auth_headers)
        return request


class BitfinexSessionLegacy(BitexSessionABC):
    """Custom Session object for the Bitfinex exchange legacy API."""

    def __init__(self, key, secret):
        """Initialize legacy session instance."""
        auth = BitfinexAuthLegacy(key, secret)
        super(BitfinexSessionLegacy, self).__init__(auth)
        self.headers = {
            "X-BFX-APIKEY": self.auth.key,
            "Content-Type": "application/json",
            "Accept": "application/json"
        }


class BitfinexSession(BitexSessionABC):
    """Custom Session object for the Bitfinex exchange API."""

    def __init__(self, key, secret):
        """Initialize a BitfinexSession instance."""
        auth = BitfinexAuthV2(key, secret)
        super(BitfinexSession, self).__init__(auth)
        self.headers = {"bfx-apikey": self.key, "content-type": "application/json"}


class BitfinexAPI(BitexAPIABC):
    """Custom API object for the Bitfinex exchange API."""

    def __init__(self, key, secret, addr=None, version=None, timeout=None, use_legacy=False):
        """Initialize a BitfinexREST instance."""
        addr = addr or 'https://api.bitfinex.com'
        version = version or 'v2'
        session_class = BitfinexSessionLegacy if use_legacy else BitfinexSession
        session = session_class(key, secret)
        super(BitfinexAPI, self).__init__(addr, version, timeout, session)
