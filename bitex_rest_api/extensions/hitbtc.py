"""HitBTC REST API backend.

Documentation available here:
    https://api.hitbtc.com/
"""

from bitex_rest_api.base import BitexAPIABC, BitexSessionABC, BitexAuthABC


class HitBTCAuth(BitexAuthABC):
    """Custom Auth class for accessing the HitBTC Exchange API."""

    def __call__(self, request):
        """Authenticate the request."""
        request.auth = (self.key, self.secret)
        return request


class HitBTCSession(BitexSessionABC):
    """Custom Session class for accessing the HitBTC Exchange API."""

    def __init__(self, key, secret):
        """Initialize the session instance."""
        auth = HitBTCAuth(key, secret)
        super(HitBTCSession, self).__init__(auth)


class HitBTCAPI(BitexAPIABC):
    """Custom API class for accessing the HitBTC Exchange API."""

    def __init__(self, key, secret, addr='http://api.hitbtc.com/api', version=None, timeout=None):
        """Initialize the API instance."""
        session = HitBTCSession(key, secret)
        super(HitBTCAPI, self).__init__(addr, version, timeout, session)
