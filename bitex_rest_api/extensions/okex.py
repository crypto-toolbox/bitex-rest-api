"""OKEX Exchange API.

Documentation at:
    https://github.com/okcoin-okex
"""
import hashlib

from bitex_rest_api.base import BitexAPIABC, BitexSessionABC, BitexAuthABC


class OKExAuth(BitexAuthABC):
    """Custom Auth class for accessing the OKEx Exchange API."""

    def __call__(self, request):
        """Authenticate the request.

        OKEx requires us to extract the body from the request, append the secret to it
        and sign it using MD5 and the secret. Finally, this needs to be added back to the
        body as a sign parameter.
        """
        decoded_body = self.decode_body(request)
        raw_signature_tuple = decoded_body + ('secret_key', self.secret)
        signature = hashlib.md5(raw_signature_tuple.encode('utf-8')).hexdigest().upper()
        request.prepare_body(decoded_body + ('sign', signature), None, None)
        return request


class OKExSession(BitexSessionABC):
    """Custom Session class for accessing the OKEx Exchange API."""

    def __init__(self, key, secret):
        """Instantiate the session instance."""
        auth = OKExAuth(key, secret)
        super(OKExSession, self).__init__(auth)
        self.headers = {'contentType': 'application/x-www-form-urlencoded'}


class OKExAPI(BitexAPIABC):
    """Custom API class for accessing the OKEx Exchange API."""

    def __init__(self, key, secret, addr='https://www.okex.com/api/', version='v1', timeout=None):
        """Instantiate the API instance."""
        session = OKExSession(key, secret)
        super(OKExAPI, self).__init__(addr, version, timeout, session)
