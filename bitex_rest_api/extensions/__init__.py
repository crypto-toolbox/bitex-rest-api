"""Supplies REST Objects for APIS."""
from bitex_rest_api.extensions.binance import BinanceAPI
from bitex_rest_api.extensions.bitfinex import BitfinexAPI
from bitex_rest_api.extensions.bitstamp import BitstampAPI
from bitex_rest_api.extensions.bittrex import BittrexAPI
from bitex_rest_api.extensions.coinbase import CoinbaseProAPI
from bitex_rest_api.extensions.hitbtc import HitBTCAPI
from bitex_rest_api.extensions.kraken import KrakenAPI
from bitex_rest_api.extensions.okex import OKExAPI
