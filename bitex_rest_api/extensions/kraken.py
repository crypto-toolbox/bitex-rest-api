"""Kraken REST API backend.

Documentation available here:
    https://www.kraken.com/en-gb/help/api
"""
# Import Built-ins
import logging
import hashlib
import hmac
import base64
import urllib
import urllib.parse
from urllib.parse import parse_qs as parse_urlencoded_params

# Import Homebrew
from bitex_rest_api.base import BitexAPIABC, BitexSessionABC, BitexAuthABC


log = logging.getLogger(__name__)


class KrakenAuth(BitexAuthABC):
    """Custom Auth class for accessing the Kraken Exchange API."""

    def add_nonce(self, request):
        """Add a nonce to the request body.

        :param requests.PreparedRequest request: the request to which the nonce is to be added.
        :rtype: Tuple[str, requests.PreparedRequest]
        """
        nonce = self.nonce()
        url_decoded_params = parse_urlencoded_params(request.body)
        url_decoded_params += ('nonce', nonce)
        new_data = urllib.parse.urlencode(url_decoded_params)

        request.prepare_body(new_data, None, None)
        return request, nonce

    def __call__(self, request):
        """Authenticate the request.

        Since private endpoints required the POST method, all data will be already urlencoded
        when we get to access it. This means we need to decode it, add the nonce and encode it
        again. Finally, we need to update the content length, as this is changed during the process.
        """
        request, nonce = self.add_nonce(request)

        nonce_and_body = (nonce + request.body).encode('utf-8')
        signature = request.url.encode('utf-8') + hashlib.sha256(nonce_and_body).digest()
        signature = hmac.new(base64.b64decode(self.secret), signature, hashlib.sha512)
        signature = base64.b64encode(signature.digest())

        # Update request kwargs
        request.headers.update({'API-Key': self.key, 'API-Sign': signature.decode('utf-8')})

        return request


class KrakenSession(BitexSessionABC):
    """Custom Session class for accessing the Kraken Exchange API."""

    def __init__(self, key, secret):
        """Initialize the Session instance."""
        auth = KrakenAuth(key, secret)
        super(KrakenSession, self).__init__(auth)


class KrakenAPI(BitexAPIABC):
    """Custom API class for accessing the Kraken Exchange API."""

    def __init__(self, key, secret, addr='https://api.kraken.com', version='0', timeout=None):
        """Initialize the API instance."""
        session = KrakenSession(key, secret)
        super(KrakenAPI, self).__init__(addr, version, timeout, session)
