"""CoinbasePro REST API backend.

Documentation available here:
    https://docs.pro.coinbase.com/#api
"""
# Import Built-ins
import logging
import hashlib
import hmac
import base64
import time

# Import Third-Party

# Import Homebrew
from bitex_rest_api.base import BitexAPIABC, BitexSessionABC, BitexAuthABC


log = logging.getLogger(__name__)


class CoinbaseProAuth(BitexAuthABC):
    """Custom Auth class to access the CoinbasePro exchange API."""

    def __init__(self, key, secret, passphrase):
        """Initialize the auth instance."""
        super(CoinbaseProAuth, self).__init__(key, secret)
        self.passphrase = passphrase

    @staticmethod
    def nonce():
        """Create a custom nonce.

        Coinbase requires seconds since epoch resolution only.
        """
        return str(time.time())

    def __call__(self, request):
        """Authenticate the passed request."""
        timestamp = self.nonce()
        body = request.body.decode('utf-8') if isinstance(request.body, bytes) else request.body
        body = body or ''
        message = (timestamp + request.method + request.path_url + body)
        hmac_key = base64.b64decode(self.secret_as_bytes)
        signature = hmac.new(hmac_key, message.encode('utf-8'), hashlib.sha256)
        signature_b64 = base64.b64encode(signature.digest())
        request.headers.update(
            {
                'CB-ACCESS-SIGN': signature_b64,
                'CB-ACCESS-TIMESTAMP': timestamp,
                'CB-ACCESS-KEY': self.key,
                'CB-ACCESS-PASSPHRASE': self.passphrase,
                'Content-Type': 'application/json'
            }
        )
        return request


class CoinbaseProSession(BitexSessionABC):
    """Custom Session class to access the CoinbasePro exchange API."""

    def __init__(self, key, secret, passphrase):
        """Initialize the session instance."""
        auth = CoinbaseProAuth(key, secret, passphrase)
        super(CoinbaseProSession, self).__init__(auth)

    @property
    def passphrase(self):
        """Return the passphrase from the auth instance."""
        return self.auth.passphrase

    @passphrase.setter
    def passphrase(self, value):
        """Set the passphrase on the auth instance."""
        self.auth.passphrase = value


class CoinbaseProAPI(BitexAPIABC):
    """Custom API class to access the CoinbasePro exchange API."""

    def __init__(self, key, secret, passphrase, addr='https://api.pro.coinbase.com/', version='',
                 timeout=None):
        """Initialize the API instance."""
        session = CoinbaseProSession(key, secret, passphrase)
        super(CoinbaseProAPI, self).__init__(addr, version, timeout, session)
