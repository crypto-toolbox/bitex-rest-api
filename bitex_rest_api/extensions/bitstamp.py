"""Bitstamp REST API backend.

Documentation available here:
    https://www.bitstamp.net/api/
"""
# Import Built-ins
import logging
import hashlib
import hmac

# Import Third-Party

# Import Homebrew
from bitex_rest_api.base import BitexAuthABC, BitexSessionABC, BitexAPIABC

log = logging.getLogger(__name__)


class BitstampAuth(BitexAuthABC):
    """Custom Auth class to access the Bitstamp Exchange API."""

    def __init__(self, key, secret, user_id):
        """Initialize the auth instance.

        :param str key: api key.
        :param str secret: api secret key.
        :param str user_id: user id of the account you want to use.
        """
        super(BitstampAuth, self).__init__(key, secret)
        self.user_id = user_id

    def __call__(self, request):
        """Sign the request.

        Authenticated messages are POST requests, so make sure to pass the parameters to the data
        kwarg.
        """
        nonce = self.nonce()
        message = nonce + self.user_id + self.key
        signature = hmac.new(self.secret.encode('utf-8'),
                             message.encode('utf-8'), hashlib.sha256)
        signature = signature.hexdigest().upper()

        request.data['key'] = self.key
        request.data['nonce'] = nonce
        request.data['signature'] = signature

        return request


class BitstampSession(BitexSessionABC):
    """Custom session class for the Bitstamp exchange API."""

    def __init__(self, key, secret, user_id):
        """Initialize the session instance."""
        auth = BitstampAuth(key, secret, user_id)
        super(BitstampSession, self).__init__(auth)

    @property
    def user_id(self):
        """Return the user id used for authentication."""
        return self.auth.user_id

    @user_id.setter
    def user_id(self, value):
        """Set the user id used for authentication."""
        self.auth.user_id = value


class BitstampAPI(BitexAPIABC):
    """Custpom API class to access the Bitstamp Exchange API."""

    def __init__(self, key, secret, user_id, addr='www.bitstamp.net/api', version='v2',
                 timeout=None):
        """Initialize API instance."""
        session = BitstampSession(key, secret, user_id)
        super(BitstampAPI, self).__init__(addr, version, timeout, session)

    def generate_url(self, endpoint):
        """Generate a Unique Resource Locator for bittstamp.

        Some endpoints are not available under version v2, hence we require the endpoint to
        explicitly begin with 'api/', if it is desired to access v1 api endpoints.
        """
        if endpoint.startswith('api/'):
            return self.addr.joinpath(endpoint.strip('api/'))
        return super(BitstampAPI, self).generate_url(endpoint)
