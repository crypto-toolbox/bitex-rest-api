import fileinput
import re

feature_regex = re.compile('^    \[FEATURE-#([1-9]|[1-9][0-9]|[1-9][0-9][0-9])\]')
patch_regex = re.compile('^    \[PATCH-#([1-9]|[1-9][0-9]|[1-9][0-9][0-9])\]')
release_regex = re.compile('^    \[RELEASE-(\d|\d\d|\d\d\d).(\d|\d\d|\d\d\d)\]')
patch_release_regex = re.compile('^    \[RELEASE-(\d|\d\d|\d\d\d).(\d|\d\d|\d\d\d)\]\[PATCH\]')
version_regex = re.compile('(\d|\d\d).(\d|\d\d).(\d|\d\d)')
version_timeline = []
log_entries = {}

with fileinput.input() as f_input:
    lines = [line.strip('\n') for line in f_input]
    entries = {'features': [], 'fixes': []}
    current_version = 'Next Release'
    """Parse the git log input via pipe."""
    for line in lines:
        if feature_regex.match(line):
            entries['features'].append(line.strip('    ').replace('[SKIP-CI]', ''))
        elif patch_regex.match(line):
            entries['fixes'].append(line.strip('    ').replace('[SKIP-CI]', ''))
        elif patch_release_regex.match(line):
            log_entries[current_version] = entries
            version_timeline.append(current_version)
            current_version = version_regex.search(line).group(0)
            entries = {'features': [], 'fixes': []}
        elif release_regex.match(line):
            log_entries[current_version] = entries
            version_timeline.append(current_version)
            current_version = version_regex.search(line).group(0)
            entries = {'features': [], 'fixes': []}

"""Next, Convert the log entries into a pretty, yet simple changelog format::

    [RELEASE-X.Y] -> Version X.Y.0
    [RELEASE-X.Y][PATCH] -> Version X.Y.1 (Patch Release)
    [RELEASE-X.Y][PATCH] -> Version X.Y.2 (Patch Release)
    (etc)
    
    [FEATURE-#XYZ] COMMIT_TITLE -> Implemented #XYZ - COMMIT_TITLE
    [PATCH-#XYZ] COMMIT_TITLE -> Fixed #XYZ - COMMIT_TITLE

The final result should then look something like this::

    >cat CHANGELOG
    Version 1.0
    ===========

    Features
    --------
    - Implemented #142 - Add a new arm to body.
    - Implemented # 182 - Add a new feature.

    Fixes
    -----
    - Fixed #521 - A terrible bug.
    
    Version 1.0.1 (Patch Release)
    =============================
    
    Fixes
    -----
    - Fixed #666 - A necessary Hotfix.
    
    [...]
"""


def convert_version(version_string):
    if patch_release_regex.match(version_string):
        version_tag, patch = version_string.split('][PATCH]')
        *_, patch = patch.split('-')
    else:
        version_tag = version_string[:-1]
        patch = None
    *_, version_tag = version_tag.split('[RELEASE-')
    major, minor = version_tag.split('.')

    if patch:
        return "Version {}.{}.{} (Patch Release)\n".format(major, minor, patch)
    else:
        return "Version {}.{}.0\n".format(major, minor)


def convert_features(features_list):
    converted_features = []
    for feature in features_list:
        # [FEATURE-#XYZ] COMMIT_TITLE -> Implemented #XYZ - COMMIT_TITLE
        tag, title = feature.split('] ')
        *_, tag = tag.split('FEATURE-')
        converted_features.append("- Implemented {} - {}\n".format(tag, title))
    return converted_features


def convert_fixes(fix_list):
    converted_fixes = []
    for patch in fix_list:
        # [PATCH-#XYZ] COMMIT_TITLE -> Implemented #XYZ - COMMIT_TITLE
        tag, title = patch.split('] ')
        *_, tag = tag.split('PATCH-')
        converted_fixes.append("- Issue {} - {}\n".format(tag, title))
    return converted_fixes


with open('CHANGELOG', 'w+') as f:
    f.write("#CHANGELOG\n")
    for version in version_timeline:
        if version != 'Next Release':
            if version[-1] == '0':
                formatted_version = "{}".format(version)
            else:
                formatted_version = "{} (Patch Release)".format(version)
        else:
            formatted_version = version
        pretty_features = convert_features(log_entries[version]['features'])
        pretty_fixes = convert_fixes(log_entries[version]['fixes'])
        f.write('{}\n{}\n\n'.format(formatted_version, '='*len(formatted_version)))
        if pretty_features:
            f.write('Features\n--------\n')
            for feature in pretty_features:
                f.write(feature)
            f.write('\n')
        if pretty_fixes:
            f.write('Fixes\n-----\n')
            for fix in pretty_fixes:
                f.write(fix)
            f.write('\n')
    if log_entries[version_timeline[1]]['features']:
        print('minor')
    else:
        print('patch')

