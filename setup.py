from setuptools import setup, find_packages

VERSION = '2.0.0a1'

setup(name='bitex-rest-api',
      version=VERSION,
      author='Nils Diefenbach',
      author_email='23okrs20+pypi@mykolab.com',
      url="gitlab.com:crypto-toolbox/bitex-rest-api.git",
      test_suite='pytest', tests_require=['pytest'],
      packages=find_packages(exclude=['contrib', 'docs', 'tests*', 'travis']),
      install_requires=['requests', 'pyjwt', 'pytz'],
      description='Python3-based API Framework for Crypto Exchanges',
      license='MIT',  classifiers=['Development Status :: 4 - Beta',
                                   'Intended Audience :: Developers'],
      )

