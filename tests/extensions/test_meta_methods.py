"""Test the implementation of ABC methods and properties of the base classes for each extension.

As the methods are required in the extending API class, we test them in a single location.
Specialized tests for any other kind of customization are located in a dedicated test file on a
per-exchange basis.

For each exchange, there needs to be a test data dictionary which provides the required test data
for each meta method test.

Here is an example of such a test data dict::

    binance_test_data = {
        'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},

        # Test data required for AUTH class tests.
        'auth_test_data': {

            # Auth class to test.
            'auth_class': BinanceAuth,

            # The following may be empty if you only need key and secret attributes. Any additional
            # credentials or attributes must be stated here.
            'expected_attr_values': {},

            # Expected PreparedRequest objects, categorized by HTTP method.
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

        # Test data required for Session class tests.
        'session_test_data': {

                # Session class to test.
                'session_class': BinanceSession,

                # Auth class expected to be assigned to session.auth
                'expected_auth_class': BinanceAuth,

                # Any customized attributes of the session object and their expected values.
                'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},

                # Expected PreparedRequest objects, categorized by HTTP method.
                'expected_PreparedRequest_attr_values': {
                    'POST': {
                        'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                        'url': 'https://exchange.com/wicked/path',
                        'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                    },
                    'GET': {
                        'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                        'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                    },
                }
            },

        # Test data required for API class tests.
        'api_test_data': {

            # API class to test.
            'api_class': BinanceAPI,

            # The expected Session class initialized and stored at API.private_connection.
            'expected_session_class': BinanceSession,

            # Any custom API attributes with their expected values.
            'expected_attr_values': {},

            # A str or dict or urls to expect. If a single str is given, we assume the generated url
            # should be identical for all HTTP methods. If a dict is passed, where the keys are
            # valid HTTP versb, the relevant url is used for assertion.
            'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
        }
    }

.. Note::

    The keys **MUST** be named exactly as given above, otherwise the test will fail.

"""
import pytest
import requests

from unittest import mock
from urllib.parse import urlencode

from bitex_rest_api.extensions import (
    BinanceAPI,
    BitfinexAPI,
    BitstampAPI,
    BittrexAPI,
    CoinbaseProAPI,
    HitBTCAPI,
    KrakenAPI,
    OKExAPI,
)

from bitex_rest_api.extensions.binance import BinanceAuth, BinanceSession
from bitex_rest_api.extensions.okex import OKExAuth, OKExSession
from bitex_rest_api.extensions.bitfinex import BitfinexAuthV2, BitfinexSession
from bitex_rest_api.extensions.bitstamp import BitstampAuth, BitstampSession
from bitex_rest_api.extensions.bittrex import BittrexAuth, BittrexSession
from bitex_rest_api.extensions.coinbase import CoinbaseProAuth, CoinbaseProSession
from bitex_rest_api.extensions.hitbtc import HitBTCAuth, HitBTCSession
from bitex_rest_api.extensions.kraken import KrakenAuth, KrakenSession


binance_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': BinanceAuth,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': BinanceSession,
            'expected_auth_class': BinanceAuth,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': BinanceAPI,
        'expected_session_class': BinanceSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}

bitfinex_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': BitfinexAuthV2,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': BitfinexSession,
            'expected_auth_class': BitfinexAuthV2,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': BitfinexAPI,
        'expected_session_class': BitfinexSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}

bitstamp_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': BitstampAuth,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': BitstampSession,
            'expected_auth_class': BitstampAuth,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': BitstampAPI,
        'expected_session_class': BitstampSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}

bittrex_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': BittrexAuth,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': BittrexSession,
            'expected_auth_class': BittrexAuth,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': BittrexAPI,
        'expected_session_class': BittrexSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}

coinbase_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': CoinbaseProAuth,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': CoinbaseProSession,
            'expected_auth_class': CoinbaseProAuth,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': CoinbaseProAPI,
        'expected_session_class': CoinbaseProSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}

hitbtc_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': HitBTCAuth,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': HitBTCSession,
            'expected_auth_class': HitBTCAuth,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': HitBTCAPI,
        'expected_session_class': HitBTCSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}

kraken_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': KrakenAuth,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': KrakenSession,
            'expected_auth_class': KrakenAuth,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': KrakenAPI,
        'expected_session_class': KrakenSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}

okex_test_data = {
    'credentials_as_dict': {'key': 'super_key', 'secret': 'super_secret'},
    'auth_test_data': {
        'auth_class': OKExAuth,
        'expected_attr_values': {},
        'expected_PreparedRequest_attr_values': {
            'POST': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path',
                'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
            },
            'GET': {
                'headers': {'standard_header': '100'},
                'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
            },
        }
    },

    'session_test_data': {
            'session_class': OKExSession,
            'expected_auth_class': OKExAuth,
            'expected_attr_values': {'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8')}},
            'expected_PreparedRequest_attr_values': {
                'POST': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path',
                    'data': urlencode([('get_pizza', True), ('vegan', False), ('signature', '86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b')]),
                },
                'GET': {
                    'headers': {'X-MBA-APIKEY': 'super_key'.encode('utf-8'), 'standard_header': '100'},
                    'url': 'https://exchange.com/wicked/path?get_pizza=True&vegan=False&signature=86d3c59e406d5ad6eb3cb30656f9aa3d12f7e6e85721f25fd428cac1d816b01b',
                },
            }
        },

    'api_test_data': {
        'api_class': OKExAPI,
        'expected_session_class': OKExSession,
        'expected_attr_values': {},
        'expected_urls_by_methods': 'https://api.binance.com/api/wicked/path',
    }
}


test_data_dicts = [
    binance_test_data,
    bitfinex_test_data,
    bitstamp_test_data,
    bittrex_test_data,
    coinbase_test_data,
    hitbtc_test_data,
    kraken_test_data,
    okex_test_data,
]


def unpack_data_dict(data_dict, key):
    """Return the data of the given data_dict at the given key."""
    keys_for_values = {
        'auth_test_data':
            ['auth_class', 'expected_attr_values', 'expected_PreparedRequest_attr_values'],
        'session_test_data':
            ['session_class', 'expected_auth_class', 'expected_attr_values'],
        'api_test_data':
            ['api_class', 'expected_session_class', 'expected_attr_values']
    }
    data = [data_dict[key][sub_key] for sub_key in keys_for_values[key]]
    data.append(data_dict['credentials_as_dict'])
    return data


@pytest.fixture
def build_prep_request_test_dummy():
    def make_dummy(method):
        if method in ('GET', 'DELETE'):
            kwargs = dict(method=method, url='https://exchange.com/wicked/path', headers={'standard_header': '100'},
                                params=[('get_pizza', True), ('vegan', False)])
        else:
            kwargs = dict(method=method, url='https://exchange.com/wicked/path', headers={'standard_header': '100'},
                                data=[('get_pizza', True), ('vegan', False)])
        r = requests.PreparedRequest()
        r.prepare(**kwargs)
        return r
    return make_dummy


@pytest.fixture
def build_request_test_dummy():
    def make_dummy(method):
        if method in ('GET', 'DELETE'):
            kwargs = dict(method=method, url='https://exchange.com/wicked/path', headers={'standard_header': '100'},
                                params={'get_pizza': True, 'vegan': False})
        else:
            kwargs = dict(method=method, url='https://exchange.com/wicked/path', headers={'standard_header': '100'},
                                data={'get_pizza': True, 'vegan': False})
        return requests.Request(**kwargs)
    return make_dummy


@pytest.mark.parametrize('test_data_dict', test_data_dicts)
def test_auth_class_init_method(test_data_dict):
    """Test that the AUTH initializes with the expected default values set."""
    auth_class, expected_attr_values, _, credentials = unpack_data_dict(test_data_dict, 'auth_test_data')

    instance = auth_class(**credentials)
    for cred, value in credentials.items():
        assert getattr(instance, cred) == value
    for attr, value in expected_attr_values.items():
        assert getattr(instance, attr) == value


@pytest.mark.parametrize('test_data_dict', test_data_dicts)
def test_auth_call_method(build_prep_request_test_dummy, test_data_dict):
    """Test the __call__ method of the given AUTH class.

    Here, we assert that the Auth.__call__() method updates the given requests.PreparedRequest
    instance as expected. We'll use a static input dictionary to create the instance, and use the
    expected values from the test_data_dict to assert the correctness.
    """
    auth_class, expected_attr_values, expected_PreparedRequest_attr_values, credentials = unpack_data_dict(
        test_data_dict, 'auth_test_data'
    )

    for METHOD in ['POST', 'GET', 'PUT', 'DELETE', 'HEAD', 'PATCH', 'OPTIONS']:
        if METHOD in expected_PreparedRequest_attr_values:
            instance = auth_class(**credentials)

            given = build_prep_request_test_dummy(METHOD)

            expected = requests.PreparedRequest()
            expected.prepare(**expected_PreparedRequest_attr_values[METHOD])
            actual = instance(given)
            if not any(key in expected_PreparedRequest_attr_values[METHOD] for key in actual.__dict__):
                raise AssertionError("None of the attributes have the expected value.")
            for attr, expected_value in expected_PreparedRequest_attr_values[METHOD].items():
                if attr == 'headers':
                    for expected_header, expected_header_value in expected_PreparedRequest_attr_values[METHOD]['headers'].items():
                        actual_headers = actual.headers
                        assert expected_header in actual_headers
                        assert actual_headers.get(expected_header) == expected_header_value
                    continue
                elif attr == 'data':
                    attr = 'body'
                assert getattr(actual, attr) == expected_value
            #
            # for attr, actual_value in actual.__dict__.items():
            #     if attr != 'headers':
            #         assert expected_PreparedRequest_attr_values[METHOD].get(attr, actual_value) == actual_value
            #     else:
            #         for header, header_value in expected_PreparedRequest_attr_values[METHOD].get(attr, {}).items():
            #             assert header in actual_value
            #             assert actual_value[header] == header_value


@pytest.mark.parametrize('test_data_dict', test_data_dicts)
def test_init_of_session_class(test_data_dict):
    """Test that the SESSION initializes with the expected default values set."""
    session_class, expected_auth_class, expected_attr_values, credentials = unpack_data_dict(test_data_dict, 'session_test_data')
    instance = session_class(**credentials)
    assert isinstance(instance.auth, expected_auth_class)
    for attr, value in credentials.items():
        assert getattr(instance, attr) == value

    for attr, value in expected_attr_values.items():
        assert getattr(instance, attr) == value


@pytest.mark.parametrize('test_data_dict', test_data_dicts)
def test_session_class_prepare_method(build_request_test_dummy, test_data_dict):
    """Assert that the prepared request object returned by the session has the expected values."""
    session_class, expected_attr_values, expected_PreparedRequest_attr_values, credentials = unpack_data_dict(
        test_data_dict, 'session_test_data'
    )

    for METHOD in ['POST', 'GET', 'PUT', 'DELETE', 'HEAD', 'PATCH', 'OPTIONS']:
        if METHOD in expected_PreparedRequest_attr_values:
            instance = session_class(**credentials)

            given = build_request_test_dummy(METHOD)

            expected = requests.PreparedRequest()
            expected.prepare(**expected_PreparedRequest_attr_values[METHOD])

            actual = instance.prepare_request(given)

            assert actual == expected


@pytest.mark.parametrize('test_data_dict', test_data_dicts)
def test_api_class_init_method(test_data_dict):
    """Test that the API initializes with the expected default values set."""
    api_class, expected_session_class, expected_attr_values, credentials = unpack_data_dict(test_data_dict, 'api_test_data')
    instance = api_class(**credentials)
    assert isinstance(instance.private_connection, expected_session_class)
    for attr, value in expected_attr_values.items():
        assert getattr(instance, attr) == value
    for attr, value in credentials.items():
        assert getattr(instance.private_connection, attr) == value


@pytest.mark.parametrize('test_data_dict', test_data_dicts)
def test_api_class_request_method(test_data_dict):
    api_class, expected_attr_values, expected_urls_by_method, credentials = unpack_data_dict(
        test_data_dict, 'api_test_data'
    )

    for METHOD in ['POST', 'GET', 'PUT', 'DELETE', 'HEAD', 'PATCH', 'OPTIONS']:
        if METHOD in expected_urls_by_method:
            if isinstance(expected_urls_by_method, dict):
                expected_url = expected_urls_by_method[METHOD]
            elif isinstance(expected_urls_by_method, str):
                expected_url = expected_urls_by_method
            else:
                raise AssertionError("'expected_urls_by_methods' must be str or dict!")

            instance = api_class(**credentials)
            instance.private_connection = mock.MagicMock(spec=requests.Session)
            instance.public_connection = mock.MagicMock(spec=requests.Session)
            expected_params = sorted(list({'hello': 'there', 'all': False}.items()))

            if METHOD in ('GET', 'DELETE'):
                instance._request(METHOD, 'wicked/path', private=True, params={'hello': 'there', 'all': False})
            else:
                instance._request(METHOD, 'wicked/path', private=True, data={'hello': 'there', 'all': False})
            assert instance.private_connection.request.called
            assert not instance.public_connection.request.called
            args, kwargs = instance.private_connection.request.call_args
            if METHOD in ('GET', 'DELETE'):
                assert kwargs.get('params') == expected_params
            else:
                assert kwargs.get('data') == expected_params
            assert kwargs.get('url') == expected_url

            instance.private_connection.reset_mock()
            instance.public_connection.reset_mock()
            if METHOD in ('GET', 'DELETE'):
                instance._request(METHOD, 'wicked/path', private=False, params={'hello': 'there', 'all': False})
            else:
                instance._request(METHOD, 'wicked/path', private=False, data={'hello': 'there', 'all': False})
            assert instance.private_connection.request.called
            assert not instance.public_connection.request.called
            args, kwargs = instance.public_connection.request.call_args
            if METHOD in ('GET', 'DELETE'):
                assert kwargs.get('params') == expected_params
            else:
                assert kwargs.get('data') == expected_params
            assert kwargs.get('url') == expected_url