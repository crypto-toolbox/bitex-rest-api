"""Base Class TestCase definitions."""
# Import Built-Ins
import logging
import os
import pathlib
from os import getcwd
from unittest import mock

# Import Third-Party
import pytest
import requests

# Import Homebrew
from bitex_rest_api.base import BitexAPIABC, BitexSessionABC, BitexAuthABC
from bitex_rest_api.exceptions import IncompleteAPIConfigurationWarning
from bitex_rest_api.exceptions import IncompleteCredentialConfigurationWarning
from bitex_rest_api.exceptions import IncompleteCredentialsWarning

# Init Logging Facilities
log = logging.getLogger(__name__)

try:
    tests_folder_dir = os.environ['CI_PROJECT_PATH'] + '/tests/'
except KeyError:
    tests_folder_dir = getcwd()

"""FIXTURES"""


# BitexAuthABC Fixtures
@pytest.fixture
def api_credentials():
    return {'secret': 'super_secret', 'key': 'super_key'}


@pytest.fixture
def base_auth_class():
    class TestBaseAuth(BitexAuthABC):
        def __init__(self, key, secret):
            super(TestBaseAuth, self).__init__(key, secret)

        def __call__(self, *args, **kwargs):
            return super(TestBaseAuth, self).__call__(*args, **kwargs)
    return TestBaseAuth


@pytest.fixture
def base_auth_instance(api_credentials, base_auth_class):
    return base_auth_class(**api_credentials)


# BitexSessionABC Fixtures
@pytest.fixture
def base_session_class():
    class TestBitexSession(BitexSessionABC):
        def __init__(self, auth):
            super(TestBitexSession, self).__init__(auth)
    return TestBitexSession


@pytest.fixture
def base_session_instance(base_session_class, base_auth_instance):
    return base_session_class(base_auth_instance)


# BitexAPIABC Fixtures
@pytest.fixture
def base_api_sans_session_config():
    return {'addr': 'http://www.supersite.com/api', 'version': 'v1',
            'timeout': 12, 'session': None}


@pytest.fixture
def base_api_con_session_config(base_api_sans_session_config, base_session_instance):
    with_session = base_api_sans_session_config.copy()
    with_session.update({'session': base_session_instance})
    return with_session


@pytest.fixture
def base_api_class():
    class TestBitexAPIABC(BitexAPIABC):
        def __init__(self, *args, **kwargs):
            super(TestBitexAPIABC, self).__init__(*args, **kwargs)
    return TestBitexAPIABC


@pytest.fixture
def base_api_instance(base_api_class, base_api_con_session_config):
    return base_api_class(**base_api_con_session_config)


"""TESTS"""


@pytest.mark.BitexAuthABC
@pytest.mark.core
def test_bitex_auth_init_method(base_auth_class):
    auth = base_auth_class('key', 'secret')
    assert auth.key == 'key'
    assert auth.secret == 'secret'


@pytest.mark.BitexAuthABC
@pytest.mark.core
def test_bitex_auth_properties(base_auth_class):
    auth = base_auth_class('key', 'secret')
    assert auth.key_as_bytes == 'key'.encode('utf-8')
    assert auth.secret_as_bytes == 'secret'.encode('utf-8')


@pytest.mark.BitexAuthABC
@pytest.mark.core
def test_bitex_auth_call_method(base_auth_instance):
    assert base_auth_instance('unmodified_object') == 'unmodified_object'


@pytest.mark.BitexAuthABC
@pytest.mark.core
@mock.patch('bitex_rest_api.base.time.time', return_value=1000.1000)
def test_base_auth_nonce_method(mock_time):
    assert BitexAuthABC.nonce() == str(int(round(1000.1000 * 1000)))
    assert mock_time.called


@pytest.mark.BitexSessionABC
@pytest.mark.core
def test_bitex_session_init_method(base_session_class):
    with pytest.raises(TypeError):
        base_session_class('Not a requests.auth.AuthBase instance')
    auth = requests.auth.AuthBase()
    session = base_session_class(auth)
    assert session.auth == auth


@pytest.mark.BitexSessionABC
@pytest.mark.core
def test_bitex_sessions_properties(base_session_instance):
    """Assert that key and secret are fetched from the auth object of the session.

    Also ensure that when setting the key and secret attributes via the session, the corresponding
    *_as_bytes attributes are updated as well.
    """
    auth = base_session_instance.auth

    assert base_session_instance.key == auth.key
    assert base_session_instance.secret == auth.secret

    base_session_instance.key = 'Wabaloo'
    assert auth.key == 'Wabaloo'

    base_session_instance.secret = 'Dabaloo'
    assert auth.secret == 'Dabaloo'


@pytest.mark.BitexAPIABC
@pytest.mark.core
def test_init_method(base_api_class, base_api_con_session_config, base_api_sans_session_config):
    """Assert correct assignment of values and call of load_config."""
    with pytest.warns(None):
        base_api_instance = base_api_class(**base_api_con_session_config)
    assert base_api_instance.addr == pathlib.Path(base_api_con_session_config['addr'])
    assert base_api_instance.version == pathlib.Path(base_api_con_session_config['version'])
    assert base_api_instance.timeout == base_api_con_session_config['timeout']
    assert isinstance(base_api_instance.private_connection, BitexSessionABC)
    assert isinstance(base_api_instance.public_connection, requests.Session)

    with pytest.warns(IncompleteCredentialConfigurationWarning):
        assert base_api_sans_session_config['session'] is None
        base_api_class(**base_api_sans_session_config)


@pytest.mark.BitexAPIABC
@pytest.mark.core
def test_generate_url_method(base_api_instance, base_api_con_session_config):
    addr = base_api_con_session_config['addr']
    version = base_api_con_session_config['version']
    endpoint = 'hello/whatsup'
    expected_result = pathlib.Path(addr).joinpath(version, endpoint)
    assert base_api_instance.generate_url(endpoint) == expected_result
    assert base_api_instance.generate_url(pathlib.Path(endpoint)) == expected_result


@pytest.mark.BitexAPIABC
@pytest.mark.core
@mock.patch('bitex_rest_api.base.requests.Session')
@mock.patch('bitex_rest_api.base.BitexAPIABC.generate_url', return_value='https://expected_url')
def test_request_method(mock_url, mock_session, base_session_class, base_api_class, base_api_sans_session_config):
    mock_bitex_session = mock.MagicMock(spec=BitexSessionABC)
    base_api_sans_session_config['session'] = mock_bitex_session

    given_dict = {'nothing': 0}
    expected = [('nothing', 0)]

    api = base_api_class(**base_api_sans_session_config)
    api._request('METHOD', 'some/endpoint', private=False, data=given_dict)
    expected_kwargs = {'url': 'https://expected_url', 'data': expected,
                       'timeout': base_api_sans_session_config['timeout'], 'method': 'METHOD'}
    mock_session().request.assert_called_once_with(**expected_kwargs)

    api._request('METHOD', 'some/endpoint', private=True, data=given_dict)
    mock_bitex_session.request.assert_called_with(**expected_kwargs)


@pytest.mark.BitexAPIABC
@pytest.mark.core
@mock.patch('bitex_rest_api.base.requests.Session.request')
@mock.patch('bitex_rest_api.base.BitexAPIABC.generate_url', return_value='https://expected_url')
def test_request_method_converts_data_and_params_to_list_of_tuples(mock_url, mock_request, base_api_instance):
    """Assert that dictionaries passed to `params` and `data` are converted to a list of tuples.

    This conversion is required to guarantee order of key=value pairs, since some exchanges need
    the parameters urlencoded and signed with the secret key. If the order were not retained
    for these exchanges, authentication may fail.
    """
    instance = base_api_instance
    given = {'get_pizza': True, 'vegan': False, 'ninja': 'turtle'}
    expected = sorted(list(given.items()), key=lambda x: x[0])
    instance._request('GET', 'endpoint', params=given, data=given)
    mock_request.assert_called_with(method='GET', url='https://expected_url', params=expected, data=expected, timeout=instance.timeout)


@pytest.mark.BitexAPIABC
@pytest.mark.core
@mock.patch('bitex_rest_api.base.BitexAPIABC._request')
def test_http_methods(mock_request, base_api_instance):
    test_data = [
        ('POST', base_api_instance.post), ('PUT', base_api_instance.put),
        ('GET', base_api_instance.get),
        ('DELETE', base_api_instance.delete),
        ('HEAD', base_api_instance.head),
        ('PATCH', base_api_instance.patch),
        ('OPTIONS', base_api_instance.options),
    ]
    endpoint = 'fake_endpoint'
    request_kwargs = {'greeting': 'hello', 'number': 123}
    for verb, method in test_data:
        method(endpoint, **request_kwargs)
        mock_request.assert_called_with(verb, endpoint, False, **request_kwargs)
        method(endpoint, private=True, **request_kwargs)
        mock_request.assert_called_with(verb, endpoint, True, **request_kwargs)
        assert mock_request.call_count == 2
        mock_request.reset_mock()



